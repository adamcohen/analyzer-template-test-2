FROM golang:1.17-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
RUN go build -o /analyzer

FROM node:14-alpine3.12

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer"]
