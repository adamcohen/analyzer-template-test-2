# analyzer-template-test changelog

## v2.12.17
- Bug fix (!14)
- Another bug fix (!14)

## v2.12.16
- Bug fix (!13)
- Another bug fix (!13)

## v2.12.15
- Bug fix (!12)
- Another bug fix (!12)

## v2.12.14
- Bug fix (!11)
- Another bug fix (!11)

## v2.12.13
- Bug fix (!10)
- Another bug fix (!10)

## v2.12.12
- Bug fix (!9)
- Another bug fix (!9)

## v2.12.11
- Bug fix (!9)

## v2.12.10
- Bug fix (!8)

## v2.12.9
- Bug fix (!7)

## v2.12.8
- Bug fix (!6)

## v2.12.7
- Bug fix (!5)

## v2.12.6
- Bug fix (!4)

## v2.12.4
- Bug fix (!3)

## v2.12.3
- Another important change (!2)

## v2.12.2
- Some important change (!1)

## v2.12.1
- Fix a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE`
